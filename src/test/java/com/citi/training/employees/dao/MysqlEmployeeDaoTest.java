package com.citi.training.employees.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MysqlEmployeeDaoTest {

	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(-1, "Conor", 25000));
		
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	@Test
	@Transactional
	public void test_findByID() {
		Employee test1 = mysqlEmployeeDao.create(new Employee(-1, "Conor", 25000));
		Employee test2 = mysqlEmployeeDao.create(new Employee(-1, "Fergal", 25000));
		
		assertThat(test1).isEqualToComparingFieldByField(mysqlEmployeeDao.findById(1));
		
	}
	
	@Test
	@Transactional
	public void test_deleteById() {
		mysqlEmployeeDao.create(new Employee(3000, "Conor", 2300));
		mysqlEmployeeDao.create(new Employee(-32, "fergal", 23200));
		mysqlEmployeeDao.deleteById(1);
		
		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	
	
}
