package com.citi.training.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebEmployeesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebEmployeesApplication.class, args);
    }

}
